import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo';
import Login from './modules/Login';
import Error from './modules/Error';

const App = () => {
  const connectionRendering = () => {
    const netInfo = useNetInfo();
    let connectionFunnel;

    if (netInfo.isConnected == true) {
      connectionFunnel = <Login/>
    } else {
      connectionFunnel = <Error errorMessage="You need a stable internet connection to run Hopper." />
    }

    return(
      <View>
        {connectionFunnel}
      </View>
    )
  }
  
  return(
    <View style={styles.container}>
        {connectionRendering()}
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  }
});

export default App;