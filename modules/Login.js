import React, { Component } from 'react';
import { Dimensions, Image, Keyboard, StyleSheet, Text, TextInput, View } from 'react-native';
import axios from 'axios';
import HopperWeb from './HopperWeb';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // SET TO EMPTY STRING VALUES WHEN NO LONGER TESTING
            inputUsername: 'elon@jumper.com',
            inputPassword: '123456',
            submitted: false,
            userData: null
        }
    };

    handleSubmit = e => {
        this.setState({submitted: true});
        this.checkUserCredentials(this.state.inputUsername, this.state.inputPassword);
    };

    checkUserCredentials = (username, password) => {
        axios
            // WILL NEED TO REPLACE WITH PERMANENT ENDPOINT
            .post('http://aa13ce61a096.ngrok.io/api/v1/auth/login', {
                username: username,
                password: password
            })
            .then(response => {
                this.setState({userData: response.data});
            })
            .catch(error => {
                alert('ERROR: Unable to log you in at this time. Please try again later.');
            })
    };
        
    render() {
        if (this.state.submitted === true && this.state.userData !== null) {
            return (
                <>
                    <HopperWeb 
                        userId={JSON.stringify(this.state.userData.tenantId)} 
                        customerId={JSON.stringify(this.state.userData.hopper.customerId)} 
                        userType={JSON.stringify(this.state.userData.hopper.userType)} 
                    />
                </>
            )
        } else {
            return(
                <>
                    <Image 
                        source={require('../assets/logo/JMP-100_LogoFinal.png')} 
                        style={loginStyles.logo}
                    />
                    <View>
                        <Text style={loginStyles.title}>Welcome to Hopper</Text>
                    </View>

                    <View>
                        <TextInput
                            autoCapitalize='none'
                            keyboardType='email-address'
                            onChangeText={(inputUsername) => this.setState({inputUsername})}
                            placeholder='Email' 
                            returnKeyType='done'
                            style={loginStyles.input}
                            value={this.state.inputUsername}
                        />
                        <TextInput 
                            autoCapitalize='none'
                            keyboardType='default'
                            onBlur={Keyboard.dismiss}
                            onChangeText={(inputPassword) => this.setState({inputPassword})}
                            placeholder='Password'
                            returnKeyType='done'
                            secureTextEntry={true}
                            style={loginStyles.input}
                            value={this.state.inputPassword}
                        />
                        <TouchableOpacity 
                            accessibilityLabel='Login to your Hopper account' 
                            onPress={this.handleSubmit} 
                            style={loginStyles.loginBtn}
                        >
                            <Text style={loginStyles.loginTxt}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>

                    {/* FUTURE GOAL TO IMPLEMENT, NOT FOR NOW */}
                    {/* <TouchableOpacity>
                        <Text style={loginStyles.forgotTxt}>Forgot Password?</Text>
                    </TouchableOpacity> */}
                </>
            )
        };
    };
};

const window = Dimensions.get('window');

const loginStyles = StyleSheet.create({
    forgotTxt: {
        fontSize: 12,
        marginTop: 10,
    },
    input: {
        borderBottomWidth: 1,
        borderColor: '#000',
        fontSize: 16,
        marginBottom: 20,
        paddingVertical: 20,
    },
    loginBtn: {
        alignItems: 'center',
        backgroundColor: '#ED3F34',
        borderColor: '#000',
        borderRadius: 5,
        borderWidth: 1,
        display: 'flex',
        justifyContent: 'center',
        marginTop: 20,
        padding: 10,
    },
    loginTxt: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'normal',
    },
    logo: {
        alignSelf: 'center',
        marginBottom: 20,
        resizeMode: 'contain',
        width: window.width * .5,
    }, 
    title: {
        color: '#000',
        fontFamily: 'Verdana',
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 30,
    }
});

export default Login;