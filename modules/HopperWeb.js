import React, { Component } from 'react';
import { ActivityIndicator, BackHandler, Dimensions, Platform, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import axios from 'axios';
import Login from './Login';

class HopperWeb extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tenantInfo: null,
            loggedOut: false
        }
    }
    
    webView = {
        canGoBack: false,
        ref: null,
    }
    
    onAndroidBackPress = () => {
        if (this.webView.canGoBack && this.webView.ref) {
            this.webView.ref.goBack();
            return true;
        }
        return false;
    }
    
    componentDidMount() {
        if (Platform.OS === 'android') {
          BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
        }

        // pass tenant ID to API to pull all their info to populate webview URL
        // WILL NEED TO REPLACE WITH PERMANENT ENDPOINT
        axios.get('http://aa13ce61a096.ngrok.io/api/v1/tenants/', {
                'X-TENANT-ID': this.props.userId
            })
            .then(response => {
                // check with Rohan if i'm pulling the data from the right place
                this.setState({tenantInfo: response.data.data[0]});
            });
    }
        
    componentWillUnmount() {
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener('hardwareBackPress');
        }
    }

    hopperUrlMaker = () => {
        // confirm this with Rohan -- is this where we should be looking for?
        const hopperUrlBase = this.state.tenantInfo.subscription.lines[0].domainLookup;

        const sKey = 'H0n6b@';
        const sDate = new Date();
        const sDateM = sDate.getMonth() + 1;
        const sDateD = sDate.getDate();
        const sDateY = sDate.getFullYear();
        const uId = this.props.userType;
        const cId = this.props.customerId;

        const uniqueHopperRedirect = 'https://' + hopperUrlBase + '/customer?via=1&s=' + sKey + sDateM + sDateD + sDateY + '&u=' + uId + '&c=' + cId;

        return uniqueHopperRedirect;
    }

    checkUrl = webState => {
        // If 'logout' gets clicked, it redirects to '.../default', 
        // which we're targeting here
        if (webState.url.includes('/default')) {
            this.setState({loggedOut: true});
        }
    }        
        
    render() {
        if (this.state.tenantInfo !== null && this.state.loggedOut === false) {
            return(
                <View style={styles.container}>
                    <WebView
                        allowsBackForwardNavigationGestures 
                        automaticallyAdjustContentInsets={false}
                        domStorageEnabled={true}
                        ignoreSslError={true}
                        javaScriptEnabled={true}
                        onNavigationStateChange={(navState) => { 
                            this.webView.canGoBack = navState.canGoBack; 
                            this.checkUrl(navState);
                        }}
                        ref={(webView) => { this.webView.ref = webView; }}
                        renderLoading={() => (
                            <ActivityIndicator
                            color='#ED3F34'
                            size='large'
                            style={styles.container}
                            />
                        )}
                        scalesPageToFit={true}
                        source={{uri: this.hopperUrlMaker()}}
                        startInLoadingState={true}
                        style={{marginTop: 50}}
                    />
                </View>
            )
        } else if(this.state.loggedOut === true) {
            return(
                <Login />
            )
        } else {
            return (
                <View>
                    <Text>Logging you in</Text>
                </View>
            )
        }
    }
}

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        height: screenHeight, 
        overflow: 'hidden',
        width: screenWidth, 
        flex: 1,
        textAlign: 'center'
    }
});

export default HopperWeb;