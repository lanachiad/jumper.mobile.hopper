import React, {Component} from 'react';
import { Alert, Button, Platform, StyleSheet, Text, View } from 'react-native';

class Error extends Component {
    render() {
        return(
            <>
                <Text>{this.props.errorMessage}</Text>
                <Text>Please check your network settings and try again.</Text>
            </>
        )
    }
}

export default Error;