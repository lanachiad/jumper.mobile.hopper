import React, { Component } from 'react';
import { ActivityIndicator, BackHandler, Dimensions, Platform, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import axios from 'axios';
import HopperWeb from './HopperWeb';

class TenantParser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tenantInfo: null,
            loggedOut: false
        }
    }
    
    componentDidMount() {
        // pass tenant ID to API to pull all their info to populate webview URL
        // WILL NEED TO REPLACE WITH PERMANENT ENDPOINT
        axios.get('http://7ddd71f3ad14.ngrok.io/api/v1/tenants/', {
                'X-TENANT-ID': this.props.userId
            })
            .then(response => {
                // check with Rohan if i'm pulling the data from the right place
                this.setState({tenantInfo: response.data});
            });
    }

    hopperUrlMaker = () => {
        // confirm this with Rohan -- is this where we should be looking for?
        const hopperUrlBase = this.state.tenantInfo.subscription.lines[0].domainLookup;

        const sKey = 'H0n6b@';
        const sDate = new Date();
        const sDateM = sDate.getMonth() + 1;
        const sDateD = sDate.getDate();
        const sDateY = sDate.getFullYear();
        const uId = this.props.userType;
        const cId = this.props.customerId;

        const uniqueHopperRedirect = 'https://' + hopperUrlBase + '/customer?via=1&s=' + sKey + sDateM + sDateD + sDateY + '&u=' + uId + '&c=' + cId;

        return uniqueHopperRedirect;
    }    
        
    render() {
        if (this.state.tenantInfo !== null && this.state.loggedOut === false) {
            return(
                <View style={styles.container}>
                    <HopperWeb hopperUrl={this.hopperUrlMaker()} />

                    {/* <WebView
                        allowsBackForwardNavigationGestures 
                        automaticallyAdjustContentInsets={false}
                        domStorageEnabled={true}
                        ignoreSslError={true}
                        javaScriptEnabled={true}
                        onNavigationStateChange={(navState) => { 
                            this.webView.canGoBack = navState.canGoBack; 
                            this.checkUrl(navState);
                        }}
                        ref={(webView) => { this.webView.ref = webView; }}
                        renderLoading={() => (
                            <ActivityIndicator
                            color='#ED3F34'
                            size='large'
                            style={styles.container}
                            />
                        )}
                        scalesPageToFit={true}
                        source={{uri: this.hopperUrlMaker()}}
                        startInLoadingState={true}
                        style={{marginTop: 50}}
                    /> */}
                </View>
            )
        } else if(this.state.loggedOut === true) {
            return(
                <Login />
            )
        } else {
            return (
                <View>
                    <Text>Logging you in</Text>
                </View>
            )
        }
    }
}

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        height: screenHeight, 
        overflow: 'hidden',
        width: screenWidth, 
        flex: 1,
        textAlign: 'center'
    }
});

export default TenantParser;