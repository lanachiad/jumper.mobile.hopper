# jumper.mobile.hopper
Hopper mobile app for iOS and Android

## Libraries
* [Axios](https://aboutreact.com/react-native-axios/#Code-To-Make-HTTP-API-call-in-React-Native-using-Axios)
* [NetInfo](https://github.com/react-native-netinfo/react-native-netinfo)
* [WebView](https://github.com/react-native-webview/react-native-webview)

## Prerequisites
* Run the following in the command line: `npm install`

## For Android
* Download Android Emulator in order to run the app locally
* Run the following in the command line: `npx react-native run-android`

## For iOS
* Download XCode in order to run the app locally
* cd into '/ios' and run the following in the command line: `pod install`
* cd back into the main director and then run the following in the command line: `npx react-native run-ios` (or `yarn ios` if on Mac)